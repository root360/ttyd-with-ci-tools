FROM    tsl0922/ttyd:alpine

RUN	apk --no-cache add \
		docker-cli \
		docker-bash-completion \
		bash-completion

ADD	bashrc /root/.bashrc
